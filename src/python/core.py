#
# Imports
#

# xfw.loader
import xfw_loader.python as loader

# xfw.vfs
import xfw_vfs.python as vfs



#
# Globals
#

__PACKAGE_ID = 'org.pypi.certifi'
__CACERT_INITED = False
__CACERT_PATH   = None



#
# Public
#

def where():
    global __CACERT_INITED
    global __CACERT_PATH

    if not __CACERT_INITED:
        path_origin = '%s/python_libraries/certifi/cacert.pem' % (loader.get_mod_directory_path(__PACKAGE_ID))
        if loader.is_mod_in_realfs(__PACKAGE_ID):
            __CACERT_PATH = path_origin
        else:
            __CACERT_PATH = u'%s/%s/cacert.pem' % (loader.XFWLOADER_TEMPDIR, __PACKAGE_ID)
            vfs.file_copy(path_origin, __CACERT_PATH)

        __CACERT_INITED = True

    return __CACERT_PATH


def contents():
    with open(where(), 'r') as data:
        return data.read()
